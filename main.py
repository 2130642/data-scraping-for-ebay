from requests_html import HTMLSession
import requests
from bs4 import BeautifulSoup
from csv import writer
s = HTMLSession()
url = 'https://www.ebay.com/b/Laptops-Netbooks/175672/bn_1648276?rt=nc'

def getdata(url):
    r = s.get(url)
    soup = BeautifulSoup(r.text, 'lxml')

    productlist = soup.find_all('div', class_='s-item__image')
    productlinks = []

    for item in productlist:
        for link in item.find_all('a', href=True):
            productlinks.append(link['href'])

    print(len(productlinks))
    print(productlinks)
    with open('laptops.csv','w', encoding='utf8',newline='') as f:
        thewriter = writer(f)
        headers = ['title', 'price','condition', 'Shipping-fee']
        thewriter.writerow(headers)
        for link in productlinks:
           r = requests.get(link)
           soup = BeautifulSoup(r.content, 'html.parser')
           try:
                name = soup.find('h1', class_='it-ttl').text
                price = soup.find('span', class_='notranslate').text
                condition = soup.find('div', class_='u-flL condText').text
                shipping = soup.find('span', class_='notranslate sh-cst').text.replace('\n', '')
           except:
                name = 'no name'
                price = 'no price'
                condition = 'no condition'
                shipping = 'no shipping'

           laptop = [name, price, condition, shipping]
           thewriter.writerow(laptop)
    return soup

def getpage(url):
    r = s.get(url)
    soup = BeautifulSoup(r.text, 'lxml')
    page = soup.find('nav', {'class': 'pagination'})
    if soup.find('nav', {'class': 'pagination'}):
       pg = page.find('a', {'class': 'pagination__next icon-link'})
       link = pg.find('a', href=True)
       url = pg['href']
       return url
    else:
        return



while True:
    getdata(url)
    url = getpage(url)
    if not url:
        break
    print(url)